-- TODO: find a better way to destroy database and restart from scratch than `docker image rm <postgres>`
CREATE DATABASE url_shortener;
-- 12 characters for the slug allows enoding all numbers within int64 range
CREATE TABLE urls (id SERIAL PRIMARY KEY, long_url varchar(2048), slug varchar(12) UNIQUE, expires_at TIMESTAMP);