package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFetchBySlug(t *testing.T) {
	// GIVEN
	s := NewInMemoryStorage()
	err := s.Store("longUrl", "slug")
	assert.Nil(t, err)
	// WHEN
	longUrl, err := s.FetchLongUrlFromSlug("slug")
	assert.Nil(t, err)
	// THEN
	assert.Equal(t, "longUrl", longUrl)
}

func TestFetchByLongUrl(t *testing.T) {
	// GIVEN
	s := NewInMemoryStorage()
	err := s.Store("longUrl", "slug")
	assert.Nil(t, err)
	// WHEN
	slug, err := s.FetchSlugFromLongUrl("longUrl")
	assert.Nil(t, err)
	// THEN
	assert.Equal(t, "slug", slug)
}
