package storage

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/avast/retry-go"
	_ "github.com/lib/pq"
)

type PostgreSqlStorage struct {
	database *sql.DB
}

func NewPostgreSqlStorage() (*PostgreSqlStorage, error) {
	databaseUrl := os.Getenv("DATABASE_URL")
	if databaseUrl == "" {
		return nil, fmt.Errorf("DATABASE_URL env variable is empty")
	}
	db, err := sql.Open("postgres", databaseUrl)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres database: %w", err)
	}
	err = retry.Do(
		func() error {
			// sql.Open does not guarantee trying to connect to the database. Ping will raise connection failures.
			err = db.Ping()
			if err != nil {
				log.Printf("ping failed on postgres database: %v, retrying...\n", err)
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, fmt.Errorf("ping failed several times on postgres database: %w", err)
	}

	// Delete expired rows regularly
	go func ()  {
		for {
			log.Print("Deleting expired rows in postgres...")
			db.Exec(
				"DELETE FROM urls WHERE expires_at < $1;",
				time.Now(),
			)
			log.Println("DONE.")
			time.Sleep(24 * time.Hour)
		}
	}()

	return &PostgreSqlStorage{
		database: db,
	}, nil
}

func (s *PostgreSqlStorage) Store(longUrl string, slug string) error {
	log.Print("Storing in postgres...")
	defer log.Println("DONE.")
	s.database.Exec(
		"INSERT INTO urls(long_url,slug,expires_at) VALUES ($1, $2, $3);",
		longUrl,
		slug,
		time.Now().Add(time.Duration(DefaultConfig.HoursToLive*int64(time.Hour))),
	)
	return nil
}

func (s *PostgreSqlStorage) FetchSlugFromLongUrl(longUrl string) (slug string, err error) {
	log.Print("Fetching from postgres...")
	defer log.Println("DONE.")
	rows, err := s.database.Query("SELECT slug FROM urls WHERE long_url = $1 AND expires_at > $2;", longUrl, time.Now())
	if err != nil {
		return "", err
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&slug)
	}
	return slug, nil
}

// FetchBySlug will be called much more often that FetchByLongUrl and should be more efficient
func (s *PostgreSqlStorage) FetchLongUrlFromSlug(slug string) (longUrl string, err error) {
	log.Print("Fetching from postgres...")
	defer log.Println("DONE.")
	rows, err := s.database.Query("SELECT long_url FROM urls WHERE slug = $1 AND expires_at > $2;", slug, time.Now())
	if err != nil {
		return "", err
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&longUrl)
	}
	return longUrl, nil
}
