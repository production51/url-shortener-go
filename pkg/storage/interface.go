package storage

import "time"

type Storage interface {
	Store(longUrl string, slug string) error
	FetchSlugFromLongUrl(longUrl string) (string, error)
	FetchLongUrlFromSlug(slug string) (string, error)
}

type Row struct {
	LongUrl   string
	Slug      string
	ExpiresAt time.Time // Unnecessary to check at fetch, run a background job to delete expired URLs
}
