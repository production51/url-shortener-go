package storage

import (
	"time"
)

type InMemoryStorage struct {
	// key: slug
	storage map[string]*Row
}

func NewInMemoryStorage() *InMemoryStorage {
	return &InMemoryStorage{
		storage: make(map[string]*Row),
	}
}

func (s *InMemoryStorage) Store(longUrl string, slug string) error {
	// Overwrite existing with newer dates
	s.storage[slug] = &Row{
		LongUrl:   longUrl,
		Slug:      slug,
		ExpiresAt: time.Now().Add(time.Duration(DefaultConfig.HoursToLive * int64(time.Hour))),
	}
	return nil
}

func (s *InMemoryStorage) FetchSlugFromLongUrl(longUrl string) (string, error) {
	for _, row := range s.storage {
		if row.LongUrl == longUrl {
			return row.Slug, nil
		}
	}
	return "", nil
}

// FetchBySlug will be called much more often that FetchByLongUrl and should be more efficient
func (s *InMemoryStorage) FetchLongUrlFromSlug(slug string) (string, error) {
	row, ok := s.storage[slug]
	if ok {
		return row.LongUrl, nil
	}
	return "", nil
}
