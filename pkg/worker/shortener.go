package worker

import (
	"fmt"
	"math/rand"
	"production/marcbrun/url-shortener/pkg/storage"

	base62 "github.com/jcoene/go-base62"
)

func (w *Worker) GetOrSetSlug(longUrl string) (slug string, err error) {
	slug, err = w.storage.FetchSlugFromLongUrl(longUrl)
	if err != nil {
		return "", fmt.Errorf("error while fetching in storage: %w", err)
	}
	if slug != "" {
		// URL is known
		return slug, nil
	}
	// URL is unknown
	// Use slug unicity constraint on database and retry upon storage until the generated slug is new
	for i := 0; i<storage.DefaultConfig.StoreRetries; i++ {
		slug = w.generateSlug()
		err = w.storage.Store(longUrl, slug)
		if err == nil {
			break
		}
	}
	if err != nil {
		return "", fmt.Errorf("error while storing: %w", err)
	}
	
	return slug, nil
}

func (w *Worker) generateSlug() string {
	slug := base62.Encode(rand.Int63())
	return slug
}
