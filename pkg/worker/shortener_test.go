package worker

import (
	"production/marcbrun/url-shortener/pkg/storage"
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestGetOrSetSlug(t *testing.T) {
	// GIVEN a test worker
	w := NewWorker(storage.NewInMemoryStorage())
	// WHEN we get or set a slug from a new longUrl
	slug1, err := w.GetOrSetSlug("https://gitlab.com/production51/url-shortener-go/-/jobs/3492613588")
	// THEN it generates a new one
	assert.Nil(t, err)

	// WHEN we get or set a slug from a known longUrl
	slug2, err := w.GetOrSetSlug("https://gitlab.com/production51/url-shortener-go/-/jobs/3492613588")
	// THEN it returns the same slug and does not generate a new one
	assert.Nil(t, err)
	assert.Equal(t, slug1, slug2)
}
