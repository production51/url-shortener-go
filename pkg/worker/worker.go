package worker

import (
	"production/marcbrun/url-shortener/pkg/storage"
)

type Worker struct {
	storage storage.Storage
}

func NewWorker(storage storage.Storage) *Worker {
	return &Worker{
		storage: storage,
	}
}
