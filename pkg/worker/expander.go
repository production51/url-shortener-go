package worker

import "fmt"

func (w *Worker) Expand(slug string) (longUrl string, found bool, err error) {
	longUrl, err = w.storage.FetchLongUrlFromSlug(slug)
	if err != nil {
		return "", false, fmt.Errorf("error while fetching in storage: %w", err)
	}
	if longUrl != "" {
		// URL is known
		return longUrl, true, nil
	}
	// URL is unknown
	return "", false, nil
}
