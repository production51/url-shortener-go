package api

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUnknown(t *testing.T) {
	// GIVEN

	// WHEN
	req := httptest.NewRequest(http.MethodGet, "https://mydomain.com/unknownSlug", nil)
	slug, err := ValidateGet(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, "unknownSlug", slug)
}

func TestPostUnknown(t *testing.T) {
	// GIVEN

	// WHEN
    r := PostRequest{LongUrl: "https://devcenter.heroku.com/articles/connecting-heroku-postgres#connection-permissions"}
    req := r.ToHttpTestReq()
	longUrl, err := ValidatePost(req)

	// THEN
	assert.Nil(t, err)
	assert.Equal(t, "https://devcenter.heroku.com/articles/connecting-heroku-postgres#connection-permissions", longUrl)
}