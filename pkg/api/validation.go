package api

import (
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

func ValidateGet(r *http.Request) (slug string, err error) {
	slug = strings.TrimPrefix(r.URL.EscapedPath(), "/")
	if len(slug) > 12 {
		return "", fmt.Errorf("slug is too long: %s", slug)
	}
	if !is_alphanum(slug) {
		return "", fmt.Errorf("slug must contain only alphanumeric characters: %s", slug)
	}
	return slug, nil
}

func is_alphanum(word string) bool {
	return regexp.MustCompile(`^[a-zA-Z0-9]*$`).MatchString(word)
}

func ValidatePost(r *http.Request) (longUrl string, err error) {
	err = r.ParseForm()
	if err != nil {
		return "", fmt.Errorf("could not parse request body: %w", err)
	}
	longUrl = r.PostFormValue("url")
	_, err = url.ParseRequestURI(longUrl)
	if err != nil {
		return "", fmt.Errorf("request body url field is not a valid URI: %w", err)
	}
	return longUrl, nil
}
