package api

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
)

// No GetRequest struct since there is no body to decode from

type PostRequest struct {
	LongUrl string `json:"url"`
}

func (r *PostRequest) ToHttpTestReq() *http.Request {
    data := url.Values{}
    data.Set("url", r.LongUrl)

    req := httptest.NewRequest(http.MethodPost, "https://mydomain.com", strings.NewReader(data.Encode()))
    req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	return req
}

type PostSuccessResponse struct {
	ShortUrl string `json:"url"`
}

func (r *PostSuccessResponse) Write(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	msg, err := json.Marshal(r)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(msg)
}

type ErrorResponse struct {
	httpStatus int
	Message    string `json:"error"`
}

func (r *ErrorResponse) Write(w http.ResponseWriter) {
	w.WriteHeader(r.httpStatus)
	msg, err := json.Marshal(r)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(msg)
}
