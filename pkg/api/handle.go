package api

import (
	"fmt"
	"log"
	"net/http"
	"production/marcbrun/url-shortener/pkg/worker"
)

type Handler struct {
	worker *worker.Worker
}

func NewHandler(worker *worker.Worker) *Handler {
	return &Handler{
		worker: worker,
	}
}

func (h *Handler) Handle(w http.ResponseWriter, r *http.Request) {
	defer log.Println("Call handled.")
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "GET":
		// user clicked on a short URL like https://<my-domain>/<slug>
		log.Println("GET call received.")
		slug, err := ValidateGet(r)
		if err != nil {
			log.Println(fmt.Errorf("invalid request: %w", err))
			response := &ErrorResponse{
				httpStatus: http.StatusBadRequest,
				Message:    "Could not expand short URL: invalid request.",
			}
			response.Write(w)
			return
		}
		longURL, found, err := h.worker.Expand(slug)
		if err != nil {
			log.Panicln(fmt.Errorf("error while expanding short URL: %w", err))
			response := &ErrorResponse{
				httpStatus: http.StatusInternalServerError,
				Message:    "Could not expand short URL: internal error.",
			}
			response.Write(w)
			return
		}
		if !found {
			response := &ErrorResponse{
				httpStatus: http.StatusNotFound,
				Message:    "Could not expand short URL: it is unknown from our services.",
			}
			response.Write(w)
			return
		}
		http.Redirect(w, r, longURL, http.StatusSeeOther)
	case "POST":
		// user posted a long URL to shorten
		// Example: `curl -X POST -d '{"url":"https://www.domain.com/path"}' my_domain:8080`
		log.Println("POST call received.")
		longURL, err := ValidatePost(r)
		if err != nil {
			log.Println(fmt.Errorf("invalid request: %w", err))
			response := &ErrorResponse{
				httpStatus: http.StatusBadRequest,
				Message:    "Could not shorten URL: invalid request.",
			}
			response.Write(w)
			return
		}
		slug, err := h.worker.GetOrSetSlug(longURL)
		if err != nil {
			log.Panicln(fmt.Errorf("error while shortening URL: %w", err))
			response := &ErrorResponse{
				httpStatus: http.StatusInternalServerError,
				Message:    "Could not shorten URL: internal error.",
			}
			response.Write(w)
			return
		}
		response := &PostSuccessResponse{
			ShortUrl: fmt.Sprintf("%s/%s", r.Host, slug),
		}
		response.Write(w)
	default:
		response := &ErrorResponse{
			httpStatus: http.StatusBadRequest,
			Message:    "Unsupported method",
		}
		response.Write(w)
	}
}
