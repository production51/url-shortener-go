# Backend Software Engineer

# Introduction

We want to build an **URL Shortener** service (only the API). Th API can be used to turn a long URL into a tiny URL.

A long URL might look like:

> [https://medium.com/equify-tech/the-three-fundamental-stages-of-an-engineering-career-54dac732fc74](https://medium.com/equify-tech/the-three-fundamental-stages-of-an-engineering-career-54dac732fc74)
> 

And the service would turn it into a tiny URL that could look like this:

> *https://<my-domain>/<slug>*
> 

***<my-domain>*** would be the domain of the API. When the user types this URL in its browser it is automatically redirected to the original URL via an HTTP redirect.

***<slug>*** would be a random short string. (eg. aY2Pv8)

# Requirements

## Must have

- Build the API in Go or Python
- Use a SQL storage
- Dockerize the API
- Tests
- Think of edge cases and make sure your solution is robust. We will **challenge** you on this point, you don’t have to implement the perfect solution, but you should be able to **pitch** your vision in under 5 minutes.

## Liberties

- The **random strings** generation does not have to be cryptographically secured

## Nice to have

If you have time you may go beyond the assignment. But this is not a feature contest !

- Same URL always returns the same generated string
- Expiration dates on URLs
- Clicks counter

# What we will look for

Writing the code should be very straight forward, we are more interested in how you communicate, what is your vision, and how you solve issues.

- Can you explain your decision process?
- What would you do to go to production?
- How would you implement more complex features?