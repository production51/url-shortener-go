# Build image
FROM golang:1.19 AS build-stage

WORKDIR /app
COPY . .

RUN CGO_ENABLED=0 go build -o /go/bin/url-shortener ./cmd/

# Actual prod image
FROM alpine:latest
WORKDIR /app

COPY --from=build-stage /go/bin/url-shortener /go/bin/url-shortener
# PostgreSQL initialization: script is executed at postgres container start if no database pre-exists
COPY init.sql /docker-entrypoint-initdb.d/init.sql