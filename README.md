# URL Shortener
This repository follows the [diataxis](https://diataxis.fr/) reference for documentation.

## How-To
To use the `url-shortener`, follow these steps on your command line:
1. Clone the repository: `git clone git@gitlab.com:production51/url-shortener-go.git`
2. Launch the API server locally: `docker compose up`
3. On another terminal tab, post a long URL to shorten: `curl -X POST -d '{"url":"https://www.google.com/"}' localhost:8080`
4. Copy-paste the short URL in the response
5. Send a GET request, following redirects, to this URL, e.g. `curl localhost:8080/6dykW3VqLje`
6. Success! :tada:

## Explanation
See the [wiki](https://gitlab.com/production51/url-shortener-go/-/wikis/home) for architecture and behavior details.