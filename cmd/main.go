package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"production/marcbrun/url-shortener/pkg/api"
	"production/marcbrun/url-shortener/pkg/storage"
	"production/marcbrun/url-shortener/pkg/worker"
)

func main() {
	log.Println("Starting URL Shortener...")
	storage, err := storage.NewPostgreSqlStorage()
	if err != nil {
		log.Fatalf("could not init storage: %v", err)
	}
	worker := worker.NewWorker(storage)
	apiHandler := api.NewHandler(worker)
	http.HandleFunc("/", apiHandler.Handle)
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("PORT env variable is empty")
	}
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
