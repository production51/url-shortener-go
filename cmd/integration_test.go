package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"production/marcbrun/url-shortener/pkg/api"
	"production/marcbrun/url-shortener/pkg/storage"
	"production/marcbrun/url-shortener/pkg/worker"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUnknown(t *testing.T) {
	// GIVEN
	storage := storage.NewInMemoryStorage()
	worker := worker.NewWorker(storage)
	apiHandler := api.NewHandler(worker)
	w := httptest.NewRecorder()

	// WHEN
	req := httptest.NewRequest(http.MethodGet, "https://mydomain.com/unknownSlug", nil)
	apiHandler.Handle(w, req)

	// THEN
	resp := w.Result()
	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, 404, resp.StatusCode)
	errorResp := api.ErrorResponse{}
	err := json.Unmarshal(body, &errorResp)
	assert.Nil(t, err)
	assert.Equal(t, "Could not expand short URL: it is unknown from our services.", errorResp.Message)
}

func TestPostUnknown(t *testing.T) {
	// GIVEN
	storage := storage.NewInMemoryStorage()
	worker := worker.NewWorker(storage)
	apiHandler := api.NewHandler(worker)
	w := httptest.NewRecorder()

	// WHEN
	r := api.PostRequest{LongUrl: "https://devcenter.heroku.com/articles/connecting-heroku-postgres#connection-permissions"}
    req := r.ToHttpTestReq()
	apiHandler.Handle(w, req)

	// THEN
	resp := w.Result()
	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, 200, resp.StatusCode)
	successResp := api.PostSuccessResponse{}
	err := json.Unmarshal(body, &successResp)
	assert.Nil(t, err)
	assert.True(t, strings.HasPrefix(successResp.ShortUrl, "mydomain.com/"))
}

func TestGetKnown(t *testing.T) {
	// GIVEN
	storage := storage.NewInMemoryStorage()
	worker := worker.NewWorker(storage)
	apiHandler := api.NewHandler(worker)
	w := httptest.NewRecorder()

	// WHEN
	err := storage.Store("https://devcenter.heroku.com/articles/connecting-heroku-postgres#connection-permissions", "7hVDYqgOFhM")
	assert.Nil(t, err)

    req := httptest.NewRequest(http.MethodGet, "https://mydomain.com/7hVDYqgOFhM", nil)
	apiHandler.Handle(w, req)

	// THEN
	resp := w.Result()

	assert.Equal(t, 303, resp.StatusCode)
	assert.Equal(t, "https://devcenter.heroku.com/articles/connecting-heroku-postgres#connection-permissions", resp.Header.Get("Location"))
}