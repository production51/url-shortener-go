module production/marcbrun/url-shortener

go 1.18

require (
	github.com/avast/retry-go v3.0.0+incompatible // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jcoene/go-base62 v0.0.0-20170519195839-4f4155803613 // indirect
	github.com/lib/pq v1.10.7 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
